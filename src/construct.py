from ursina import *
import json

def construct_object(objec, bud):
    info = {}

    with open(f"models/{objec}.json", "r") as file:
        info = json.load(file)
    
    for i in info.get("define"):
        tmp_entity = Entity(parent = bud)

        tmp_entity.model = i.get("model")
        tmp_entity.color = color.rgb(i.get("color")[0], i.get("color")[1], i.get("color")[2])
        tmp_entity.scale = Vec3(i.get("scale")[0], i.get("scale")[1], i.get("scale")[2])
        tmp_entity.position = Vec3(i.get("position")[0], i.get("position")[1], i.get("position")[2])
