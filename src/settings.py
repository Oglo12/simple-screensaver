import json

DEFAULTS = {
        "object": "default",
        "motion": [2, 2, 0],
        "trigger": 0.1,
        }

def get_setting(setting):
    info = {}

    de_val = DEFAULTS.get(setting)

    try:
        with open("settings.json", "r") as file:
            info = json.load(file)
    except:
        return de_val

    if info.get(setting) == None:
        return de_val

    else:
        return info.get(setting)
