from ursina import *
from ursina.shaders import basic_lighting_shader
from random import randint

from construct import *
from settings import *

#### SETTINGS ####
mouse_trigger = get_setting("trigger")
objec = get_setting("object")
motion = Vec3(get_setting("motion")[0], get_setting("motion")[1], get_setting("motion")[2])
##################

mouse_stop = False

window.fullscreen = True

application.development_mode = False

mouse.visible = False

app = Ursina()

window.color = color.rgb(0, 0, 0)

bud = Entity(shader = basic_lighting_shader)
bud.parent = scene
bud.position = Vec3(0, 0, 0)

construct_object(objec, bud)

camera.position = bud.position
camera.z -= 15

new_rot = bud.rotation

prev_time = time.time()

bsize = window.size * 0.003

bounds = [(-bsize[0], bsize[0]), (-bsize[1], bsize[1]), (-10, 1)]

init_time = time.time()

def input(key):
    global mouse_stop
    
    if key == 'm':
        mouse_stop = not mouse_stop

    elif key == 'q':
        exit();

def update():
    global bud, new_rot, prev_time, motion, bounds, mouse_trigger, init_time

    bud.rotation = lerp(bud.rotation, new_rot, 3 * time.dt)
    bud.position += motion * time.dt

    if time.time() - prev_time > 0.1:
        new_rot = bud.rotation + Vec3(45, 45, 0)

        prev_time = time.time()

    if mouse_stop == True:
        mouse.position = Vec3(0, 0, 0)

    if time.time() - init_time > 1:
        if abs(mouse.x) > mouse_trigger or abs(mouse.y) > mouse_trigger:
            if mouse_stop == True:
                exit()

    for i in range(0, len(bounds)):
        if bud.position[i] < bounds[i][0] or bud.position[i] > bounds[i][1]:
            motion[i] *= -1

app.run()
